#!/bin/bash

GITLAB_URL="https://gitlab.com"

gitlab_get_path_from_git_origin() {
    if [ -f '.git' ] ; then
        # @TODO make this independent of domain name
        echo $(git remote get-url --all origin) \
            | sed 's/git@gitlab.com://g' \
            | sed 's/https:\/\/gitlab.com//g' \
            | sed 's/.git$//g'
    fi
}

gitlab_get_encoded_path_from_git_origin() {
    gitlab_encode_project_path "$(gitlab_get_path_from_git_origin)"
}

gitlab_encode_project_path() {
    echo $1 | sed 's/\//%2F/g'
}

gitlab_curl_api() {
    method=${1:-}; shift
    path=${1:-}; shift
    if [ -z $gitlab_access_token ] ; then
        gitlab_access_token=$(cat "$PATH_USER_CONFIG/gitlab_token" | jq -r .access_token)
    fi
    curl -s \
        --header "Authorization: Bearer ${gitlab_access_token}" \
        --request $method "${GITLAB_URL}/api/v4/${path}" \
        "$@"
}

gitlab_login() {
    username=$1
    password=$2
    
    if [ -z "$username" ] ; then
        read -p "Username: " username
    fi
    if [ -z "$password" ] ; then
        read -s -p "Password: " password
    fi
    if [ -z "$1" ] || [ -z "$2" ] ; then
        echo ""
    fi
    
    curl -s \
        --data-urlencode "grant_type=password" \
        --data-urlencode "username=${username}" \
        --data-urlencode "password=${password}" \
        --request POST "${GITLAB_URL}/oauth/token" > "$PATH_USER_CONFIG/gitlab_token"
        
    error="$(cat "$PATH_USER_CONFIG/gitlab_token" | jq .error)"
    if [ "$error" != "null" ] ; then
        echo "Login failed: $error"
        $(cat "$PATH_USER_CONFIG/gitlab_token" | jq .error_description)
        rm "$PATH_USER_CONFIG/gitlab_token"
        exit 1
    fi
}

gitlab_logout() {
    rm "$PATH_USER_CONFIG/gitlab_token"
}

gitlab_is_loggedin() {
    [ -f "$PATH_USER_CONFIG/gitlab_token" ] && echo "1" || echo "0"
}

gitlab_project_create() {
    project_name=$1
    if [ -z $project_name ] ; then
        error "[project_name] is required"
        exit 1
    fi
    gitlab_curl_api POST projects/ \
        --data-urlencode "name=${project_name}" \
        --data-urlencode 'visibility=private'
}

gitlab_project_delete() {
    project_path="$(gitlab_get_path_from_git_origin)"
    project_id=$(gitlab_encode_project_path "${1:-$project_path}")
    if [ -z $project_id ] ; then
        echo 'Error: [project_id] is required'
        exit 1
    fi
    gitlab_curl_api DELETE "projects/${project_id}"
}
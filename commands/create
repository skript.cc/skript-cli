#!/bin/bash

. "$PATH_SRC/lib/gitlab.sh"

usage() {
    cat <<EOF
Bootstraps a new project

Usage:
  create:project [project_name]     Creates a new project with initial files
  create:git-remote [project_name]  Creates a remote git project in Gitlab

Options:
  -d  Use default values
EOF
}

set_defaults() {
    defaults=()
    defaults+=(project_description "$project_name project")
    defaults+=(project_license "UNLICENSED")
    defaults+=(project_license_owner "skript creative coding") # @TODO this is not a good default value, get it from somewhere else
    if [ `which git` ] ; then
        defaults+=(project_author_name "$(git config user.name)")
        defaults+=(project_author_email "$(git config user.email)")
    fi
    
    local defaults_total=$(expr ${#defaults[*]} - 1)
    
    if [ $use_defaults = false ] ; then
        local default_prefix='default_'
    fi
    
    for (( i=0; i<$defaults_total; i+=2 )) ; do
        printf -v "${default_prefix}${defaults[$i]}" '%s' "${defaults[$i+1]}"
    done
}

ask_input() {
    prompt=$1
    varname=$2
    default_varname="default_$varname"
    
    read -p "$prompt [${!default_varname}]: " $varname
    if [ -z "${!varname}" ] ; then 
        printf -v "$varname" '%s' "${!default_varname}"
    fi
}

ask_yesno() {
    prompt=$1
    varname=$2
    default_varname="default_$varname"
    while true; do
        read -p "$prompt [yes/no]: " yn
        case $yn in
            [Yy]* ) answer=true;break;;
            [Nn]* ) answer=false;break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
    printf -v "$varname" '%s' $answer
}

ask_select() {
    prompt=$1
    choices=$2
    varname=$3
    default_varname="default_$varname"
    echo "$prompt: "
    select choice in $choices; do
        case $choice in
            * ) break;;
        esac
    done
    printf -v "$varname" '%s' $choice
}

ask_project_name() {
    default_project_name=$(basename $PWD)
    if [ $use_defaults = true ] ; then 
        project_name=$default_project_name
    else
        ask_input "How would you like to name this project?" project_name
    fi
}

collect_project_info() {
    # Begin with asking a project name
    ask_project_name
    
    # Set default values
    set_defaults
    
    # Ask for input when we don't use the defaults
    if [ $use_defaults = false ] ; then
        ask_input "Can you give a small description?" project_description
        ask_yesno "Are you the author?" is_project_author
        if [ $is_project_author = false ] ; then
            ask_input "What's the author's name?" project_author_name
            ask_input "What's the author's e-mail address?" project_author_email
        else
            project_author_name=$default_project_author_name
            project_author_email=$default_project_author_email
        fi
        ask_select "Which license do you want to use?" \
            "UNLICENSED GPLv3 LGPL Apache2 MIT" \
            project_license
        [ $project_license = 'MIT' ] && ask_input "Who's the license owner?" project_license_owner
    fi
}

show_project_info() {
    cat <<EOF
---------------------------------------------------------------
Name:           $project_name
Description:    $project_description
License:        $project_license
License owner:  $project_license_owner
Author:         $project_author_name <$project_author_email>
---------------------------------------------------------------
EOF
}

create_readme() {
    # Inspiration for the sections and texts comes from https://guides.github.com/features/wikis/
    cat > README.md <<EOF
# ${project_name}
${project_description}

## Installation
@TODO Tell other users how to install your project locally. Optionally, include
a gif to make the process even more clear for other people.

## Usage
@TODO Instruct other people on how to use your project after they’ve installed
it. This would also be a good place to include screenshots of your project in 
action.

## Contributing
@TODO Outline contribution instructions. If you have specific contribution
preferences, explain them so that other developers know how to best contribute
to your work.

## License
$project_license

See [LICENSE](/LICENSE) to see the full text.

EOF
}

create_changelog() {
    cat > CHANGELOG.md <<EOF
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Unreleased

EOF
}

create_license() {
    case "${project_license}" in
        "GPLv3")
            curl -s https://raw.githubusercontent.com/OpenSourceOrg/licenses/master/texts/plain/GPL-3.0 -o "$PWD/LICENSE"
            ;;
        "LGPL")
            curl -s https://raw.githubusercontent.com/OpenSourceOrg/licenses/master/texts/plain/LGPL-2.0 -o "$PWD/LICENSE"
            ;;
        "Apache2")
            curl -s https://raw.githubusercontent.com/OpenSourceOrg/licenses/master/texts/plain/Apache-2.0 -o "$PWD/LICENSE"
            ;;
        "MIT")
            curl -s https://raw.githubusercontent.com/OpenSourceOrg/licenses/master/texts/plain/MIT -o "$PWD/LICENSE"
            sed -i "s/<YEAR>/$(date +%Y)/g" "$PWD/LICENSE"
            sed -i "s/<OWNER>/${project_license_owner:-<OWNER>}/g" "$PWD/LICENSE"
            ;;
    esac
}

create_gitignore() {
    cat > .gitignore <<EOF
/.skript
EOF
}

create_project() {
    info "---> Creating initial files"
    mkdir -p '.skript'
    create_readme
    create_changelog
    create_license
    create_gitignore

    info "---> Initializing git repository"
    git init
    git add --all
    git commit -m 'Initial commit.'
    
    info "---> Creating remote gitlab repository"
    create_gitremote
    git_origin_sshurl=$(echo "$gitlab_project_json" | jq -r '.ssh_url_to_repo')
    
    info "---> Adding gitlab repository as remote origin"
    if [ "$git_origin_sshurl" != "null" ] ; then
        git remote add origin "$git_origin_sshurl"
        git push -u origin master
    else
        warning "Failed to add git remote as origin"
    fi
}

create_gitremote() {
    if [ $(gitlab_is_loggedin) = 0 ] ; then
        gitlab_login
    fi
    if [ -z "$project_name" ] ; then
        ask_project_name
    fi
    gitlab_project_json="$(gitlab_project_create "$project_name")"
}

run() {
    subcommand=${1:-'project'}; shift
    
    # Parse options
    use_defaults=false
    while getopts ":dh" o; do
        case "${o}" in
            d)
                use_defaults=true
                ;;
            \? )
                echo "Invalid Option: -$OPTARG" 1>&2
                exit 1
                ;;
            : )
                echo "Invalid Option: -$OPTARG requires an argument" 1>&2
                exit 1
                ;;
            *)
                usage
                ;;
        esac
    done
    shift $((OPTIND-1))

    case "$subcommand" in
        "git-remote")
            create_gitremote
            ;;
        "project")
            # Parse arguments
            default_project_name=$(basename $PWD)
            project_name="${1}"
            
            # Ask for info and get this shit going
            collect_project_info
            show_project_info
            
            if [ $use_defaults = false ] ; then
                ask_yesno "Does this look good to you?" goodtogo
                if [ $goodtogo = false ] ; then
                    collect_project_info
                else
                    create_project
                fi
            else
                create_project
            fi
            ;;
        *)
            usage
            ;;
    esac
}
#!/bin/bash

set -e

mkdir -p ~/.skript
curl -s https://gitlab.com/skript.cc/skript-cli/-/archive/master/skript-cli-master.tar.gz \
    | tar xzf - -C ~/.skript/
mv ~/.skript/skript-cli-master ~/.skript/src
if [ ! -f ~/.local/bin/skript ] ; then
    ln -s ~/.skript/src/skript ~/.local/bin/
fi

# Skript CLI
A bash script to automate as much as possible from project creation to deployment

## Installation
```
curl -s https://gitlab.com/skript.cc/skript-cli/raw/master/install.sh | bash
```

## Usage
Once installed run `skript -h` for usage instructions